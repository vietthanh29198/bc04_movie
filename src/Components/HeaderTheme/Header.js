import React from "react";
import UserNav from "./UserNav";

export default function Header() {
  return (
    <div className="shadow">
      {" "}
      <div className="h-20 flex justify-between items-center mx-auto container">
        <span className="text-red-500 text-4xl font-medium animate-bounce">
          HomeMovies
        </span>
        <UserNav />
      </div>
    </div>
  );
}
