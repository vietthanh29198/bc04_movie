import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  setLoadingOffAction,
  setLoadingOnAction,
} from "../../redux/actions/actionSpinner";
import { movieServ } from "../../services/moviesService";
import ItemMovie from "./ItemMovie";
import TabsMovies from "./TabsMovies";

export default function HomePage() {
  const [movies, setDataMovies] = useState([]);

  let dispatch = useDispatch();

  useEffect(() => {
    dispatch(setLoadingOnAction());
    movieServ
      .getListMovie()
      .then((res) => {
        dispatch(setLoadingOffAction());
        setDataMovies(res.data.content);
      })
      .catch((err) => {
        dispatch(setLoadingOffAction());
        console.log(err);
      });
  }, []);

  const renderMovies = () => {
    return movies.map((data, index) => {
      return <ItemMovie key={index} data={data} />;
    });
  };

  return (
    <div className="container mx-auto">
      <div className="grid grid-cols-5 gap-10 mb-10 ">{renderMovies()}</div>
      <TabsMovies />
    </div>
  );
}
