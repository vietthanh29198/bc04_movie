import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
import moment from "moment";

export default function ItemMovie({ data }) {
  const { Meta } = Card;
  return (
    <Card
      className="h-full"
      hoverable
      style={{ width: "100%" }}
      cover={
        <img
          alt="example"
          src={data.hinhAnh}
          className="h-80 w-full object-cover"
        />
      }
    >
      <Meta
        title={<p className="text-red-500 truncate text-xl">{data.tenPhim}</p>}
        description={<p className="text-sm"> {data.moTa}</p>}
      />
      <p className="text-xs">
        Khởi chiếu: {moment(data.ngayKhoiChieu).format("DD-MM-YYYY ~ hh:mm A")}
      </p>
      <NavLink to={`/detail/${data.maPhim}`}>
        <button className="w-full py-2 text-center bg-red-500 mt-5 text-white rounded transition cursor-pointer hover:bg-black  duration-300  ">
          Xem chi tiết
        </button>
      </NavLink>
    </Card>
  );
}

// biDanh: "trainwreckk"
// dangChieu: true
// danhGia: 10
// hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/trainwreckk_gp05.jpg"
// hot: false
// maNhom: "GP05"
// maPhim: 1333
// moTa: "<p>Having thought that monogamy was never possible, a commitment-phobic career woman may have to face her fears when she meets a good guy.</p>"
// ngayKhoiChieu: "2021-01-11T00:00:00"
// sapChieu: false
// tenPhim: "Trainwreckk"
// trailer: "https://www.youtube.com/embed/2MxnhBPoIx4"
