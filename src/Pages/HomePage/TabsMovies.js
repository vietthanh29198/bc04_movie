import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieServ } from "../../services/moviesService";
import ItemTabsMovie from "./ItemTabsMovie";

export default function TabsMovies() {
  const [dataMovies, setDataMovies] = useState([]);
  useEffect(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
        console.log(res);
        setDataMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderContent = () => {
    return dataMovies.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          key={index}
          tab={<img className="w-16 h-16" src={heThongRap.logo} />}
        >
          <Tabs style={{ height: 500 }} tabPosition="left">
            {heThongRap.lstCumRap.map((cumRap, index) => {
              return (
                <Tabs.TabPane
                  key={index}
                  tab={
                    <div className="w-48 text-left">
                      <p className="text-gray-700 truncate">
                        {cumRap.tenCumRap}
                      </p>

                      <p className="truncate">{cumRap.diaChi}</p>
                    </div>
                  }
                >
                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="h-50 scrollbar-thin scrollbar-thumb-blue-700 scrollbar-track-blue-300 overflow-y-scroll scrollbar-track-rounded-full"
                  >
                    {cumRap.danhSachPhim.map((phim, index) => {
                      return <ItemTabsMovie data={phim} key={index} />;
                    })}
                  </div>
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };
  return (
    <div>
      <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
        {renderContent()}
      </Tabs>
    </div>
  );
}
