import { BASE_URL, https, TOKEN_CYBERSOFT } from "./configURL";
import axios from "axios";

export const movieServ = {
  // getListMovie: () => {
  //   return axios({
  //     url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05`,
  //     method: "GET",
  //     headers: {
  //       TokenCybersoft: TOKEN_CYBERSOFT,
  //     },
  //   });
  // },
  getListMovie: () => {
    let uri = "/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01";
    return https.get(uri);
  },
  getMovieByTheater: () => {
    let uri = "/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP01";
    return https.get(uri);
  },
};
